#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>

#define MAX 80
#define CNTMAX 10

char history_tab[CNTMAX][MAX];
int wait_flag = 0;
int run_flag = 1;

void add_data_to_history(int counter, char buf[MAX])
{
    int data_length=strlen(buf);
    for(int i=0; i<data_length; i++)
    {
        history_tab[counter][i]=buf[i];
    }
}

void print_history()
{
    for(int i=0; i<CNTMAX; i++)
    {
        printf("%d", i);

        for(int j=0; j<MAX; j++)
        { 
            printf( "%c", history_tab[i][j]);
            fflush(stdout);
        }

        printf("\n");
        fflush(stdout);
    }
}

void parse_data(char* buf)
{
     int cmdlen = strlen(buf);

        if(buf[cmdlen-1] == '\n')
            buf[cmdlen-1] = '\0';

        if(buf[cmdlen-2] == '&')
        {
            buf[cmdlen-2] = '\0';
            wait_flag = 1;
        }

}

int internal_cmd(char* buf, int counter)
{
    int cmdlen= strlen(buf);
    int nbr;

    if(strncmp(buf, "exit", cmdlen) == 0)
        {
            run_flag = 0;
            exit(0);
        }

    if(strncmp(buf, "!!", cmdlen) == 0)
        {
            print_history();
            //exit(0);
        }

    if(buf[0]=='!' && buf[1]!='!')
    { 
        int nbr= buf[1]-'0';

        if(0<=nbr<10)
        {
            for(int i=0; i<MAX; i++)
            {
                buf[i]=history_tab[nbr][i];
                parse_data(buf);

                history_tab[counter][i]=buf[i];


                printf("%c", history_tab[nbr][i]);
                fflush(stdout);
            }
        }
        else
        {
            fprintf(stderr, "there's no such command in recent history!\n");
        }
    }

    return 0;
}

int main(void)
{
    //char buf[MAX];
    int pid;
    pid_t wpid;
    int cmdlen;
    char *argv[MAX+1]; //command line arguments
    int cnt=0;
    int status;

    while(run_flag)
    {
        char buf[MAX];

        int argc = 0;

        printf(">");
        fflush(NULL);

        fgets(buf,sizeof(buf), stdin);

        add_data_to_history(cnt, buf);

        parse_data(buf);

        internal_cmd(buf, cnt);

        cnt++;
        cnt=cnt%10;

         /*tokenization of the command line */
        if ((argv[argc] = strtok(buf, " \t\n")) == NULL)
            return 0;

        while (argv[argc] && argc< MAX -1)
        {
            argv[++argc] = strtok(NULL, " \t\n"); //NULL-everything in cmd
        }

        argv[argc] = NULL;
        fflush(NULL);

        pid = fork();

        if(pid < 0)
        {
            perror("child proccess not created");
        }

        if(pid == 0)
        {
            printf("kiddo\n");
            fflush(stdout);
            execvp(argv[0], argv);
            exit(0);
        }

        if(pid > 0)
        {
            if(wait_flag)
            {
                wait(NULL);
                printf("waiting for a child\n");
                fflush(NULL);
            }

            printf("child finishes\n");

            while ((wpid=waitpid(0, &status, 0)) > 0) 
            {
		        if (WIFEXITED(status)) 
                {
			    printf("Process nr %lu exited with code=%d\n", (unsigned long)wpid, WEXITSTATUS(status));
                }
            }

        }    

        sleep(1);
        fflush(NULL);

    }

}